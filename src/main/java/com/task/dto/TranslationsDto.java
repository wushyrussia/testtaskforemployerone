package com.task.dto;

import lombok.Data;
import lombok.ToString;

@Data
@ToString
public class TranslationsDto {
    String de;
    String es;
    String fr;
    String ja;
    String it;
    String br;
    String pt;
    String nl;
    String hr;
    String fa;
}
