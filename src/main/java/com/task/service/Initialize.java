package com.task.service;

import com.task.dto.CountryDto;

import java.util.List;

public interface Initialize {
     List<CountryDto> getData();
     void saveData(List<CountryDto> list);
}
