package com.task.model;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity
public class Country {
    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    Long id;
    String name;
    String topLevelDomain;
    String population;
    String latlng;
    @Column(columnDefinition="LONGVARCHAR")
    String currencies;
    @Column(columnDefinition="LONGVARCHAR")
    String languages;
    @Column(columnDefinition="LONGVARCHAR")
    String translations;
    String flag;
    @Column(columnDefinition="LONGVARCHAR")
    String regionalBlocs;
}
